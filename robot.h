#ifndef ROBOT_H
#define ROBOT_H
#define motor11 0
#define motor12 1
#define motor21 2
#define motor22 3
unsigned char forward();
unsigned char backward();
unsigned char moveLeft();
unsigned char moveRight();
#endif // ROBOT_H
