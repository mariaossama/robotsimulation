#include "robot.h"

unsigned char forward()
{
    unsigned char cs=0;
    cs|=(1<<motor11);
    cs&=~(1<<motor12);
    cs|=(1<<motor21);
    cs&=~(0<<motor22);
    return cs;
}

unsigned char backward()
{
    unsigned char cs=0;
    cs&=~(1<<motor11);
    cs|=(1<<motor12);
    cs&=~(1<<motor21);
    cs|=(1<<motor22);
    return cs;
}
unsigned char moveLeft()
{
    unsigned char cs=0;
    cs&=~(1<<motor11);
    cs&=~(1<<motor12);
    cs|=(1<<motor21);
    cs&=~(1<<motor22);
    return cs;
}
unsigned char moveRight()
{
    unsigned char cs=0;
    cs|=(1<<motor11);
    cs&=~(1<<motor12);
    cs&=~(1<<motor21);
    cs&=~(1<<motor22);
    return cs;
}
