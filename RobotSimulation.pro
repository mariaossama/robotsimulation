#-------------------------------------------------
#
# Project created by QtCreator 2019-07-06T09:55:45
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = RobotSimulation
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    robot.cpp

HEADERS += \
    robot.h
